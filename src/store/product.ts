import apiClient from "../apiClient";

interface Product {
  // product_id: number;
  // product_code: string | unknown;
  // product_name_th: string;
  // product_name_en: string;
  // product_adult_price: number;
  // product_child_price: number;
  // product_total_price: number;
  // product_addon: number;
  // product_status: number;
}

interface ProductState {
  products: Product[];
}

const state = {
  products: [] as Product[],
};

const mutations = {
  SET_BOOK_LIST(state: ProductState, products: Product[]) {
    state.products = products;
  },
  SET_PRODUCT_DETAIL(state: any, payload: Product) {
    state.productDetail = payload;
  },
};

const getters = {
  allProducts: (state: ProductState) => state.products,
};

const actions = {
  async GET_BOOK_LIST({ commit }: any, params: any) {
    let url = `books/v1/volumes`;

    await apiClient
      .get(url,{
        params
      })
      .then((response) => {
        console.log('ddddddddd',response.data)
        // commit("SET_BOOK_LIST", response.data);
      })
      .catch((error) => {
        console.error("Failed to fetch products:", error);
      });
  },


  async getProduct_detail(context: any, data: string) {
    let url = `books/v1/volumes/${data}`;
    await apiClient
      .get(url)
      .then((res) => {
        context.commit("SET_PRODUCT_DETAIL", res.data);
        return res.data;
      })
      .catch((error) => {
        console.error("An error occurred:", error);
        return false;
      });
  },

};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
