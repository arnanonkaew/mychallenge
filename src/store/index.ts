import { createStore } from "vuex";
import product from "./product.ts";

const store = createStore({
  modules: {
    product,
  },
});

export default store;
