import axios, { AxiosInstance } from "axios";

const backendBaseURL = 'https://www.googleapis.com/';

const apiClient: AxiosInstance = axios.create({
  baseURL: backendBaseURL,
  headers: {
    "Content-type": "application/json",
  },
});

export default apiClient;
