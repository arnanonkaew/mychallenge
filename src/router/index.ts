import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    component: () => import("../views/LayoutView.vue"),
    children: [
      {
        path: "/",
        component: () => import("../views/HomeView.vue"),
      },
      {
        path: "/favorite",
        component: () => import("../views/FavoriteView.vue"),
      },
    ],
  },
  
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
